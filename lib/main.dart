import 'package:flutter/material.dart';
import 'package:pokedex/core/config/app_init.dart';
import 'package:pokedex/core/observers/app_bloc_observer.dart';

import 'core/navigation/route_navigator.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppInit.initializeApp();
  runApp(
    const MyApp(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    BlocObserverConfig.configure();
    return MaterialApp.router(
      routerConfig: router,
      title: 'Pokédex',
    );
  }
}
