import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:pokedex/feature/splash_screen/presenter/bloc/splash_screen_bloc.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SplashScreenBloc>(
      create: (context) => SplashScreenBloc()
        ..add(
          const InitialEvent(),
        ),
      child: const _Page(),
    );
  }
}

class _Page extends StatelessWidget {
  const _Page({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocListener<SplashScreenBloc, SplashScreenState>(
      listener: (context, state) {
        if (state is PushToHomeState) {
          context.goNamed('home');
        }
        return;
      },
      child: Scaffold(
        backgroundColor: const Color(0xffAA463D),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 20.0,
                ),
                child: Image.asset(
                  'assets/png/logo.png',
                ),
              ),
              const SizedBox(height: 20),
              Image.asset(
                'assets/gif/pika_run.gif',
                width: MediaQuery.sizeOf(context).width * 0.5,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
