import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'splash_screen_event.dart';
part 'splash_screen_state.dart';

class SplashScreenBloc extends Bloc<SplashScreenEvent, SplashScreenState> {
  SplashScreenBloc() : super(SplashScreenInitial()) {
    on<InitialEvent>(_onInitialEvent);
  }

  Future<void> _onInitialEvent(
    InitialEvent event,
    Emitter<SplashScreenState> emit,
  ) async {
    emit(
      const SplashLoadingState(),
    );
    await Future.delayed(
      const Duration(
        seconds: 3,
      ),
    );
    emit(
      const PushToHomeState(),
    );
  }
}
