import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:pokedex/feature/home_page/child/pokemon_page/constants/pokemon_type_constants.dart';
import 'package:pokedex/feature/home_page/child/pokemon_page/presenter/widgets/attack_wrap.dart';
import 'package:pokedex/feature/home_page/child/pokemon_page/presenter/widgets/pokemon_image.dart';
import 'package:pokedex/feature/home_page/child/pokemon_page/presenter/widgets/type_container.dart';
import 'package:pokedex/feature/home_page/data/models/pokemon_model.dart';

class PokemonPage extends StatelessWidget {
  const PokemonPage({
    super.key,
    required this.chosenPokemon,
  });

  final PokemonModel chosenPokemon;

  @override
  Widget build(BuildContext context) {
    Color firstBorderColor = PokemonTypesConstants.getColorForType(
        chosenPokemon.types.first.type.name);
    Color secondBorderColor = chosenPokemon.types.length > 1
        ? PokemonTypesConstants.getColorForType(
            chosenPokemon.types[1].type.name)
        : firstBorderColor;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Center(
          child: Text(
            '#${chosenPokemon.order}',
            style: const TextStyle(color: Colors.white),
          ),
        ),
      ),
      backgroundColor: const Color(0xff121212),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  PokemonImage(
                    pokemonTypeColor: firstBorderColor,
                    chosenPokemon: chosenPokemon,
                  ),
                  Text(
                    chosenPokemon.name,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 40,
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                  const SizedBox(height: 20),
                  TypeContainer(
                    firstBorderColor: firstBorderColor,
                    secondBorderColor: secondBorderColor,
                    chosenPokemon: chosenPokemon,
                  ),
                  const SizedBox(height: 20),
                  Text(
                    'Attacks:',
                    style: TextStyle(
                      color: firstBorderColor,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  MoveWrap(
                    chosenPokemon: chosenPokemon,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.all(16.0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.red,
                  shadowColor: Colors.transparent,
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  elevation: 0,
                ),
                onPressed: () {
                  context.pop();
                },
                child: Container(
                  width: MediaQuery.sizeOf(context).width * 0.5,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 24,
                    vertical: 12,
                  ),
                  child: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Salir',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
