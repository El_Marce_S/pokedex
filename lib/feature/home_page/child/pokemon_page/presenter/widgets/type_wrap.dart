import 'package:flutter/material.dart';
import 'package:pokedex/feature/home_page/child/pokemon_page/constants/pokemon_type_constants.dart';
import 'package:pokedex/feature/home_page/data/models/pokemon_model.dart';

class TypeWrap extends StatelessWidget {
  const TypeWrap({
    super.key,
    required this.chosenPokemon,
  });

  final PokemonModel chosenPokemon;

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 8.0,
      children: chosenPokemon.types.map(
        (type) {
          Color backgroundColor =
              PokemonTypesConstants.getColorForType(type.type.name);
          bool isBackgroundYellow = backgroundColor == Colors.yellow;
          return Chip(
            label: Text(
              type.type.name,
              style: TextStyle(
                color: isBackgroundYellow ? Colors.black : Colors.white,
                fontSize: 16,
              ),
            ),
            backgroundColor: backgroundColor,
          );
        },
      ).toList(),
    );
  }
}
