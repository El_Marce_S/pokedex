import 'package:flutter/material.dart';
import 'package:pokedex/feature/home_page/data/models/pokemon_model.dart';

class PokemonImage extends StatelessWidget {
  const PokemonImage({
    super.key,
    required this.pokemonTypeColor,
    required this.chosenPokemon,
  });

  final Color pokemonTypeColor;
  final PokemonModel chosenPokemon;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: 300,
          height: 300,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            gradient: RadialGradient(
              colors: [
                pokemonTypeColor.withOpacity(0.6),
                Colors.transparent,
              ],
              stops: const [
                0.3,
                1.0,
              ],
            ),
          ),
        ),
        Positioned(
          bottom: 80,
          child: Container(
            width: 100,
            height: 7,
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.1),
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.50),
                  blurRadius: 10,
                  spreadRadius: 7,
                  offset: const Offset(0, 0),
                ),
              ],
            ),
          ),
        ),
        Image.network(
          chosenPokemon.sprites.frontShiny,
          width: 300,
          height: 150,
        ),
      ],
    );
  }
}
