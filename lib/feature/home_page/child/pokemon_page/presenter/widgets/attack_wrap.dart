import 'package:flutter/material.dart';
import 'package:pokedex/feature/home_page/child/pokemon_page/constants/pokemon_type_constants.dart';
import 'package:pokedex/feature/home_page/data/models/pokemon_model.dart';

class MoveWrap extends StatelessWidget {
  const MoveWrap({
    super.key,
    required this.chosenPokemon,
  });

  final PokemonModel chosenPokemon;

  @override
  Widget build(BuildContext context) {
    Color pokemonTypeColor = PokemonTypesConstants.getColorForType(
        chosenPokemon.types.first.type.name);

    return Wrap(
      alignment: WrapAlignment.spaceBetween,
      spacing: 8.0,
      runSpacing: 4.0,
      children: chosenPokemon.moves
          .map(
            (move) => Container(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                border: Border.all(
                  color: pokemonTypeColor,
                  width: 1,
                ),
                color: Colors.transparent,
              ),
              child: Text(
                move.move.name,
                style: TextStyle(
                  color: pokemonTypeColor,
                ),
              ),
            ),
          )
          .toList(),
    );
  }
}
