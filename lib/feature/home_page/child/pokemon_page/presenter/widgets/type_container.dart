import 'package:flutter/material.dart';
import 'package:pokedex/feature/home_page/child/pokemon_page/presenter/widgets/type_wrap.dart';
import 'package:pokedex/feature/home_page/data/models/pokemon_model.dart';

class TypeContainer extends StatelessWidget {
  const TypeContainer({
    super.key,
    required this.firstBorderColor,
    required this.secondBorderColor,
    required this.chosenPokemon,
  });

  final Color firstBorderColor;
  final Color secondBorderColor;
  final PokemonModel chosenPokemon;

  @override
  Widget build(BuildContext context) {
    const pillHeight = 36.0;
    return Padding(
      padding: const EdgeInsets.only(
        top: pillHeight / 2,
      ),
      child: Stack(
        alignment: Alignment.topCenter,
        clipBehavior: Clip.none,
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(
                Radius.circular(20),
              ),
              border: Border.all(
                color: firstBorderColor,
                width: 2,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0).copyWith(
                top: pillHeight / 2,
              ),
              child: Container(
                margin: const EdgeInsets.all(1),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(20),
                  ),
                  border: Border.all(
                    color: secondBorderColor,
                    width: 2,
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 15,
                  ),
                  child: TypeWrap(
                    chosenPokemon: chosenPokemon,
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: -(pillHeight / 2),
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 12,
                vertical: 2,
              ),
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(15),
                border: Border.all(
                  color: firstBorderColor,
                  width: 2,
                ),
              ),
              child: Text(
                'Types',
                style: TextStyle(
                  color: firstBorderColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
