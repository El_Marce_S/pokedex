import 'package:flutter/material.dart';

class PokemonTypesConstants {
  static const List<String> types = [
    'normal',
    'fire',
    'water',
    'electric',
    'grass',
    'ice',
    'fighting',
    'poison',
    'ground',
    'flying',
    'psychic',
    'bug',
    'rock',
    'ghost',
    'dragon',
    'dark',
    'steel',
    'fairy',
    'stellar',
  ];

  static Color getColorForType(String type) {
    switch (type) {
      case 'normal':
        return Colors.brown;
      case 'fire':
        return Colors.red;
      case 'water':
        return Colors.blue;
      case 'electric':
        return Colors.yellow;
      case 'grass':
        return Colors.green;
      case 'ice':
        return Colors.lightBlue;
      case 'fighting':
        return Colors.orange;
      case 'poison':
        return Colors.purple;
      case 'ground':
        return Colors.brown;
      case 'flying':
        return Colors.blueGrey;
      case 'psychic':
        return Colors.pink;
      case 'bug':
        return Colors.greenAccent;
      case 'rock':
        return Colors.grey;
      case 'ghost':
        return Colors.purpleAccent;
      case 'dragon':
        return Colors.indigo;
      case 'dark':
        return Colors.black;
      case 'steel':
        return Colors.grey;
      case 'fairy':
        return Colors.pinkAccent;
      case 'stellar':
        return Colors.deepPurpleAccent;
      default:
        return Colors.grey;
    }
  }
}
