part of 'home_bloc.dart';

sealed class HomeState extends Equatable {
  const HomeState();
}

final class HomeInitial extends HomeState {
  @override
  List<Object> get props => [];
}

final class LoadingState extends HomeState {
  const LoadingState();

  @override
  List<Object> get props => [];
}

final class GotPokemonState extends HomeState {
  const GotPokemonState({required this.pokemon});

  final PokemonModel pokemon;

  @override
  List<Object> get props => [pokemon];
}

final class ErrorState extends HomeState {
  const ErrorState();

  @override
  List<Object> get props => [];
}
