import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pokedex/feature/home_page/data/models/pokemon_model.dart';
import 'package:pokedex/feature/home_page/domain/repository/home_repository.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc({required this.homeRepository}) : super(HomeInitial()) {
    on<HomeEvent>((event, emit) {});
    on<InitialEvent>(_onInitialEvent);
  }

  final HomeRepository homeRepository;

  Future<void> _onInitialEvent(
      InitialEvent event, Emitter<HomeState> emit) async {
    emit(
      const LoadingState(),
    );

    final result = await homeRepository.getPokemonDetails(
      pokemonName: event.pokemonToSearch,
    );
    result.when(
      ok: (data) {
        emit(
          GotPokemonState(pokemon: data),
        );
      },
      err: (err) {
        emit(
          const ErrorState(),
        );
      },
    );
  }
}
