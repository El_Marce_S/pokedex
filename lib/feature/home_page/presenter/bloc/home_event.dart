part of 'home_bloc.dart';

sealed class HomeEvent extends Equatable {
  const HomeEvent();
}

class InitialEvent extends HomeEvent {
  const InitialEvent({required this.pokemonToSearch});

  final String pokemonToSearch;

  @override
  List<Object> get props => [pokemonToSearch];
}
