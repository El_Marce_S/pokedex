import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:pokedex/feature/home_page/data/api/home_api.dart';
import 'package:pokedex/feature/home_page/presenter/bloc/home_bloc.dart';
import 'package:pokedex/feature/home_page/presenter/page/widgets/background_poke.dart';
import 'package:pokedex/feature/home_page/presenter/page/widgets/pokemon_search_bar.dart';
import 'package:pokedex/feature/home_page/presenter/page/widgets/search_button.dart';
import 'package:pokedex/shared/error_dialog.dart';
import 'package:pokedex/shared/loading_dialog.dart';
import 'package:pokedex/shared/push_notification_handler.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController controller = TextEditingController();
    return BlocProvider<HomeBloc>(
      create: (context) => HomeBloc(
        homeRepository: HomeApi(),
      ),
      child: _Page(controller: controller),
    );
  }
}

class _Page extends StatelessWidget {
  const _Page({
    required this.controller,
  });

  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return BlocListener<HomeBloc, HomeState>(
      listener: (context, state) {
        if (state is LoadingState) {
          LoadingDialog.show(context);
          return;
        }
        if (state is GotPokemonState) {
          controller.clear();
          context.pop();
          context.pushNamed(
            'pokemon-page',
            extra: {
              'chosenPokemon': state.pokemon,
            },
          );
          return;
        }
        if (state is ErrorState) {
          context.pop();
          controller.clear();
          ErrorDialog.show(context);
          return;
        }
      },
      child: Scaffold(
        backgroundColor: const Color(0xff121212),
        body: _Body(controller: controller),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({
    required this.controller,
  });

  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        const PushNotificationHandler(),
        const BackgroundPoke(),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 100,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 36),
                child: Text(
                  'Que pokemon buscas?',
                  style: TextStyle(
                    fontSize: 25,
                    color: Colors.white,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
              PokemonSearchBar(controller: controller),
              const SizedBox(
                height: 30,
              ),
              SearchButton(searchController: controller)
            ],
          ),
        ),
      ],
    );
  }
}
