import 'package:flutter/material.dart';

class BackgroundPoke extends StatelessWidget {
  const BackgroundPoke({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Positioned(
      right: -150,
      top: -30,
      child: Transform.rotate(
        angle: -0.3,
        child: SizedBox(
          width: 400,
          height: 400,
          child: Opacity(
            opacity: 0.3,
            child: Image.asset(
              'assets/png/pokeball.png',
            ),
          ),
        ),
      ),
    );
  }
}
