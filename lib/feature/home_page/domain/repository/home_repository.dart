import 'package:oxidized/oxidized.dart';
import 'package:pokedex/core/models/failure.dart';
import 'package:pokedex/feature/home_page/data/models/pokemon_model.dart';

abstract class HomeRepository {
  Future<Result<PokemonModel, Failure>> getPokemonDetails({
    required String pokemonName,
  });
}
