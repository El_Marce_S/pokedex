class SpeciesEntity {
  const SpeciesEntity({
    required this.name,
    required this.url,
  });

  final String name;
  final String url;
}
