import 'package:pokedex/feature/home_page/data/models/pokemon_model.dart';
import 'package:pokedex/feature/home_page/domain/entities/species_entity.dart';

class MoveEntity {
  final SpeciesEntity move;

  MoveEntity({
    required this.move,
  });
}