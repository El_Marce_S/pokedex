import 'package:pokedex/feature/home_page/domain/entities/species_entity.dart';

class AbilityEntity {
  const AbilityEntity({
    required this.ability,
    required this.isHidden,
    required this.slot,
  });

  final SpeciesEntity ability;
  final bool isHidden;
  final int slot;
}
