import 'package:pokedex/feature/home_page/data/models/ability_model.dart';
import 'package:pokedex/feature/home_page/data/models/move_model.dart';
import 'package:pokedex/feature/home_page/data/models/sprites_model.dart';
import 'package:pokedex/feature/home_page/data/models/type_model.dart';

class PokemonEntity {
  const PokemonEntity({
    required this.abilities,
    required this.id,
    required this.moves,
    required this.name,
    required this.order,
    required this.sprites,
    required this.types,
  });

  final List<AbilityModel> abilities;
  final int id;
  final List<MoveModel> moves;
  final String name;
  final int order;
  final SpritesModel sprites;
  final List<TypeModel> types;
}
