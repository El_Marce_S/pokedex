import 'package:pokedex/feature/home_page/data/models/species_model.dart';

class TypeEntity {
  const TypeEntity({
    required this.slot,
    required this.type,
  });

  final int slot;
  final SpeciesModel type;
}
