import 'package:dio/dio.dart';
import 'package:oxidized/oxidized.dart';
import 'package:pokedex/core/config/dio_services.dart';
import 'package:pokedex/core/models/failure.dart';
import 'package:pokedex/feature/home_page/data/models/pokemon_model.dart';
import 'package:pokedex/feature/home_page/domain/repository/home_repository.dart';

class HomeApi extends HomeRepository {
  final Dio _dioClient = BasicDioServices.instance.dioClient;

  @override
  Future<Result<PokemonModel, Failure>> getPokemonDetails({
    required String pokemonName,
  }) async {
    try {
      final response = await _dioClient.get(
        'api/v2/pokemon/$pokemonName',
      );
      if (response.statusCode == 200 && response.data != null) {
        try {
          final pokemon = PokemonModel.fromJson(response.data);
          return Ok(pokemon);
        } catch (e) {
          print(e);
          return const Err(
            ServerFailure(
              message: 'Error getting Pokemon',
            ),
          );
        }
      } else {
        return const Err(
          ServerFailure(
            message: 'Error getting Pokemon',
          ),
        );
      }
    } on DioException catch (dioError) {
      return Err(
        ServerFailure(
          message: dioError.message ?? 'Something went wrong',
        ),
      );
    } catch (e) {
      print(e);
      return Err(
        LocalFailure(
          message: e.toString(),
        ),
      );
    }
  }
}
