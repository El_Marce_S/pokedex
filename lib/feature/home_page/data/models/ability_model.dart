import 'package:pokedex/feature/home_page/data/models/species_model.dart';
import 'package:pokedex/feature/home_page/domain/entities/ability_entity.dart';

class AbilityModel extends AbilityEntity {
  const AbilityModel({
    required SpeciesModel super.ability,
    required super.isHidden,
    required super.slot,
  });

  factory AbilityModel.fromJson(Map<String, dynamic> json) {
    return AbilityModel(
      ability: SpeciesModel.fromJson(json['ability']),
      isHidden: json['is_hidden'],
      slot: json['slot'],
    );
  }
}
