import 'package:pokedex/feature/home_page/domain/entities/sprites_entity.dart';

class SpritesModel extends SpritesEntity {
  const SpritesModel({
    required super.frontShiny,
  });

  factory SpritesModel.fromJson(Map<String, dynamic> json) {
    return SpritesModel(
      frontShiny: json['official-artwork']['front_shiny'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'frontShiny': frontShiny,
    };
  }
}
