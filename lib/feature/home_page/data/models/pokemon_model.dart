import 'package:pokedex/feature/home_page/data/models/ability_model.dart';
import 'package:pokedex/feature/home_page/data/models/move_model.dart';
import 'package:pokedex/feature/home_page/data/models/sprites_model.dart';
import 'package:pokedex/feature/home_page/data/models/type_model.dart';
import 'package:pokedex/feature/home_page/domain/entities/pokemon_entity.dart';

class PokemonModel extends PokemonEntity {
  const PokemonModel({
    required super.abilities,
    required super.id,
    required super.moves,
    required super.name,
    required super.order,
    required super.sprites,
    required super.types,
  });

  factory PokemonModel.fromJson(Map<String, dynamic> json) {
    return PokemonModel(
      abilities: (json['abilities'] as List<dynamic>)
          .map((e) => AbilityModel.fromJson(e))
          .toList(),
      id: json['id'],
      moves: (json['moves'] as List<dynamic>)
          .map((e) => MoveModel.fromJson(e))
          .toList(),
      name: json['name'],
      order: json['order'],
      sprites: SpritesModel.fromJson(
        json['sprites']['other'],
      ),
      types: (json['types'] as List<dynamic>)
          .map((e) => TypeModel.fromJson(e))
          .toList(),
    );
  }
}
