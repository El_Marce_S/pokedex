import 'package:pokedex/feature/home_page/data/models/species_model.dart';
import 'package:pokedex/feature/home_page/domain/entities/type_entity.dart';

class TypeModel extends TypeEntity {
  const TypeModel({
    required super.slot,
    required super.type,
  });

  factory TypeModel.fromJson(Map<String, dynamic> json) {
    return TypeModel(
      slot: json['slot'],
      type: SpeciesModel.fromJson(json['type']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'slot': slot,
      'type': type.toJson(),
    };
  }
}
