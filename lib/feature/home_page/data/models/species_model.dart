import 'package:pokedex/feature/home_page/domain/entities/species_entity.dart';

class SpeciesModel extends SpeciesEntity {
  const SpeciesModel({
    required super.name,
    required super.url,
  });

  factory SpeciesModel.fromJson(Map<String, dynamic> json) {
    return SpeciesModel(
      name: json['name'],
      url: json['url'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'url': url,
    };
  }
}
