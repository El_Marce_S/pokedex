import 'package:pokedex/feature/home_page/data/models/species_model.dart';
import 'package:pokedex/feature/home_page/domain/entities/move_entity.dart';

class MoveModel extends MoveEntity {
  MoveModel({
    required super.move,
  });

  factory MoveModel.fromJson(Map<String, dynamic> json) {
    return MoveModel(
      move: SpeciesModel.fromJson(json['move']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'move': (move as SpeciesModel).toJson(),
    };
  }
}
