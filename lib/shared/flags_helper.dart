import 'package:pokedex/core/config/remote_config_service.dart';

class FlagsHelper {
  static Future<bool> testRemoteConfig() async {
    final RemoteConfigService remoteConfigService =
        await RemoteConfigService.getInstance();
    return remoteConfigService.getBool('testing_remote_config');
  }

  static Future<bool> secondValue() async {
    final RemoteConfigService remoteConfigService =
    await RemoteConfigService.getInstance();
    return remoteConfigService.getBool('second_value');
  }

  static Future<bool> activateReleaseNotifications() async {
    final RemoteConfigService remoteConfigService =
    await RemoteConfigService.getInstance();
    return remoteConfigService.getBool('notifications_only_on_release');
  }



}
