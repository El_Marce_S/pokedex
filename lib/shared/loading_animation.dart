import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class LoadingAnimation extends StatelessWidget {
  const LoadingAnimation({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Lottie.asset(
          'assets/lottie/squirtle.json',
          width: 200,
          height: 200,
          fit: BoxFit.cover,
          frameRate: FrameRate.max,
          repeat: true,
          animate: true,
        ),
      ),
    );
  }
}
