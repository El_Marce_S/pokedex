import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class PushNotificationHandler extends StatelessWidget {
  const PushNotificationHandler({super.key});

  @override
  Widget build(BuildContext context) {

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Nuevo mensaje: ${message.notification?.title}'),
          action: SnackBarAction(
            label: 'Cerrar',
            onPressed: () {
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
            },
          ),
        ),
      );
    });

    return SizedBox.shrink();
  }
}
