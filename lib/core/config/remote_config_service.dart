import 'package:firebase_remote_config/firebase_remote_config.dart';

class RemoteConfigService {
  final FirebaseRemoteConfig remoteConfig;

  static RemoteConfigService? _instance;

  static Future<RemoteConfigService> getInstance() async {
    if (_instance == null) {
      _instance = RemoteConfigService._(await FirebaseRemoteConfig.instance);
      await _instance!._initialize();
    }
    return _instance!;
  }

  RemoteConfigService._(this.remoteConfig);

  Future<void> _initialize() async {
    try {
      await remoteConfig.setConfigSettings(RemoteConfigSettings(
        fetchTimeout: const Duration(seconds: 10),
        minimumFetchInterval: Duration.zero,
      ));
      await remoteConfig.fetchAndActivate();
    } catch (e) {
      print(
          'Unable to fetch remote config. Cached or default values will be used due $e');
    }
  }

  bool getBool(String key) {
    return remoteConfig.getBool(key);
  }
}
