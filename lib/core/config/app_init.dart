import 'package:firebase_core/firebase_core.dart';
import 'package:pokedex/core/config/fcm_configs.dart';
import 'package:pokedex/core/config/remote_config_service.dart';
import 'package:pokedex/shared/flags_helper.dart';

class AppInit {
  static Future<void> initializeApp() async {
    const bool isRelease = bool.fromEnvironment('dart.vm.product');
    await Firebase.initializeApp();
    await RemoteConfigService.getInstance();
    await FCMConfig.initializeFCM();

    bool activateNotifications =
        await FlagsHelper.activateReleaseNotifications();
    bool shouldReceivePushNotifications = activateNotifications || !isRelease;

    print(shouldReceivePushNotifications
        ? 'Notificaciones push configuradas.'
        : 'Notificaciones push desactivadas.');

    print(isRelease
        ? 'Estamos corriendo la versión release'
        : 'Estamos corriendo la versión develop');
  }
}
