import 'dart:developer';

import 'package:dio/dio.dart';

class BasicDioServices {
  static final BasicDioServices _singleton = BasicDioServices._internal();
  late Dio dioClient;

  factory BasicDioServices() {
    if (!_singleton.dioClient.options.baseUrl.isNotEmpty) {
      _singleton.dioClient = Dio(
        BaseOptions(
          baseUrl: 'http://pokeapi.co/',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
        ),
      );
      _singleton._addLogInterceptor();
    }
    return _singleton;
  }

  BasicDioServices._internal() {
    dioClient = Dio(
      BaseOptions(
        baseUrl: 'http://pokeapi.co/',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      ),
    );
    _addLogInterceptor();
  }

  void _addLogInterceptor() {
    dioClient.interceptors.add(
      LogInterceptor(
        request: true,
        responseBody: true,
        requestHeader: true,
        responseHeader: false,
        error: true,
        requestBody: true,
        logPrint: (Object object) => log(
          object.toString(),
        ),
      ),
    );
  }

  static BasicDioServices get instance => _singleton;

  void setCustomHeader(String header, String value) {
    dioClient.options.headers[header] = value;
  }
}
