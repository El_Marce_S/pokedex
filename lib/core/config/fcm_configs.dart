import 'dart:io' show Platform;

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:pokedex/shared/flags_helper.dart';

class FCMConfig {
  static Future<void> initializeFCM() async {
    final bool activateNotifications = await FlagsHelper.activateReleaseNotifications();
    final bool isRelease = bool.fromEnvironment('dart.vm.product');

    if ((activateNotifications && isRelease) || (!activateNotifications && !isRelease) || (!activateNotifications && isRelease)) {
      if (Platform.isIOS) {
        await _requestPermissionsForIOS();
      }
      _configureBackgroundMessageHandler();
      await _subscribeToTopic();
      await _displayDeviceToken();
      _configureForegroundMessageHandler();
    }
  }

  static Future<void> _requestPermissionsForIOS() async {
    await FirebaseMessaging.instance.requestPermission(
      alert: true,
      badge: true,
      sound: true,
    );
  }

  static void _configureBackgroundMessageHandler() {
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  }

  static Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
    print("Manejando mensaje en segundo plano: ${message.messageId}");
  }

  static Future<void> _subscribeToTopic() async {
    await FirebaseMessaging.instance.subscribeToTopic('allDevices');
  }

  static Future<void> _displayDeviceToken() async {
    String? token = await FirebaseMessaging.instance.getToken();
    print("Token FCM del dispositivo: $token");
  }

  static void _configureForegroundMessageHandler() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print(
          'Mensaje en primer plano: ${message.notification?.title}, ${message.notification?.body}');
    });
  }
}
