import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:pokedex/feature/home_page/child/pokemon_page/presenter/pokemon_page.dart';
import 'package:pokedex/feature/home_page/presenter/page/home_page.dart';
import 'package:pokedex/feature/splash_screen/presenter/page/splash_screen.dart';

final GoRouter router = GoRouter(
  initialLocation: '/splash',
  routes: [
    GoRoute(
      path: '/splash',
      builder: (BuildContext context, GoRouterState state) =>
          const SplashScreen(),
    ),
    GoRoute(
      path: '/home',
      name: 'home',
      builder: (BuildContext context, GoRouterState state) => const HomePage(),
    ),
    GoRoute(
      path: '/home/pokemon-page',
      name: 'pokemon-page',
      builder: (BuildContext context, GoRouterState state) {
        final chosenPokemon =
            (state.extra as Map<String, dynamic>)['chosenPokemon'];
        return PokemonPage(chosenPokemon: chosenPokemon);
      },
    ),
  ],
);
